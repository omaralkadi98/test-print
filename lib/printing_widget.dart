import 'dart:typed_data';

import 'package:blue_thermal_printing/blue_print.dart';
import 'package:esc_pos_utils/esc_pos_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:pdfx/pdfx.dart' as pdfx;
import 'package:image/image.dart' as im;

FlutterBlue flutterBlue = FlutterBlue.instance;

class PrintingWidget extends StatefulWidget {
  const PrintingWidget({Key? key}) : super(key: key);

  @override
  _PrintingWidgetState createState() => _PrintingWidgetState();
}

class _PrintingWidgetState extends State<PrintingWidget> {
  List<ScanResult>? scanResult;

  @override
  void initState() {
    super.initState();
    findDevices();
  }

  void findDevices() {
    flutterBlue.startScan(timeout: const Duration(seconds: 4));
    flutterBlue.scanResults.listen((results) {
      setState(() {
        scanResult = results;
      });
    });
    flutterBlue.stopScan();
  }

  void printWithDevice(BluetoothDevice device) async {
    await device.connect();
    final gen = Generator(PaperSize.mm58, await CapabilityProfile.load());
    final printer = BluePrint();
    printer.add(gen.qrcode('https://altospos.com'));
    printer.add(gen.text('Hello'));
    printer.add(gen.text('World', styles: const PosStyles(bold: true)));
    printer.add(gen.feed(1));
    await printer.printData(device);
    device.disconnect();
  }

Future<Uint8List> getImage() async {
    final ByteData data = await rootBundle.load('assets/invoice.pdf');
    final Uint8List bytes = data.buffer.asUint8List();
    final document = await pdfx.PdfDocument.openData(bytes);
    final page = await document.getPage(1);
    return (await page.render(
            width: page.width * 2.5, height: page.height * 2.5))!
        .bytes;
  }

  void print(BluetoothDevice device) async {
    await device.connect();
    final gen = Generator(PaperSize.mm58, await CapabilityProfile.load());
    final printer = BluePrint();
    List<int> imageBytes = await getImage();
    final im.Image? image = im.decodeImage(imageBytes);
    printer.add(gen.image(image!));

    printer.add(gen.feed(1));
    await printer.printData(device);
    device.disconnect();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text('Bluetooth devices')),
      body: ListView.separated(
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(scanResult![index].device.name),
            subtitle: Text(scanResult![index].device.id.id),
            onTap: () => print(scanResult![index].device),
          );
        },
        separatorBuilder: (context, index) => const Divider(),
        itemCount: scanResult?.length ?? 0,
      ),
    );
  }
}
